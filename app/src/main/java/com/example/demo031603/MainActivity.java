package com.example.demo031603;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.annotation.BindView;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tv_main_content)
    TextView tv_main_content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}